import axios from 'axios';
import React, { useEffect, useRef, useState } from 'react';


import NavBar from '../navBar';
import Footer from "../footer";
import Request from './RequestPage.module.css';

export default function RequestPage() {


    const file = useRef();

    function AddCv() {
        file.current.click();
    };

    const StagiaireId = window.location.search.split('=')[1];

    useEffect(() => {
        setTimeout(() => {
            document.body.className = '';
        }, 500);
    }, []);

    
    function SendRequest(e) {
        e.preventDefault()

        
        axios.get(`http://localhost:8000/api/Stagires/Requestes/Finder/${StagiaireId}`)
        .then(data => {
                if (!data.data.success) {

                    const formData      = new FormData();
                    formData.append('Acc_id', StagiaireId)
                    formData.append('Message', e.target.elements.message.value)
                    formData.append('StagiaireCV', e.target.elements.CV.files[0])

                    axios.post('http://localhost:8000/api/Requestes', formData )
                    .finally(alert('The request has been submitted!'))
                } else {
                    alert("You already sent a request!");
                }
            });
    };

  return (
    <div id={Request.RequestPage} className='reqBody'>
      <div>
        <NavBar />  
        <div>
            <h2>Entez votre information pour demande de stage</h2>
            <form onSubmit={SendRequest}>
                <div>
                    <label htmlFor="message">Ton Message</label>
                    <textarea id='message' placeholder='Message' name='message' ></textarea>
                </div>
                <div onClick={AddCv}>
                    <input ref={file} type="file" name="CV" style={{ display: 'none' }} />
                    <div>
                        <p>Ajouter votre CV</p>
                    </div>
                </div>
                <div>
                <button type="submit" >Envoyer la demande</button>
                </div>
            </form>
        </div>
        <Footer/>
      </div>
    </div>
  );
};